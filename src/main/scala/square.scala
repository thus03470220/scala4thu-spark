import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac008 on 2017/5/1.
  */
object square extends App{
  val conf = new SparkConf().setAppName("square")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)
  val intRdd=sc.parallelize(1 to 100000)
  val square=intRdd.map(x=>x*x)
  square.take(10).foreach(println)
  square.collect().foreach(println)

}
